﻿using System;
using System.Xml;
using Business.Interfaces;
using Microsoft.XmlDiffPatch;

namespace Business.Implementation
{
    public class XmlComparer : IXmlComparer
    {
        private readonly XmlDiff xmlDiff;

        public XmlComparer(XmlDiff xmlDiff)
        {
            this.xmlDiff = xmlDiff;
        }

        public bool AreEqual(XmlReader leftXmlReader, XmlReader rightXmlReader)
        {
            if (leftXmlReader == null)
            {
                throw new ArgumentNullException("leftXmlReader");
            }
            if (rightXmlReader == null)
            {
                throw new ArgumentNullException("rightXmlReader");
            }

            var areEqual = xmlDiff.Compare(leftXmlReader, rightXmlReader);

            return areEqual;
        }

        public bool CompareXml(XmlReader leftXmlReader, XmlReader rightXmlReader, XmlWriter diffgramWriter)
        {
            if (leftXmlReader == null)
            {
                throw new ArgumentNullException("leftXmlReader");
            }

            if (rightXmlReader == null)
            {
                throw new ArgumentNullException("rightXmlReader");
            }

            var areIdentical = xmlDiff.Compare(leftXmlReader, rightXmlReader, diffgramWriter);

            return areIdentical;
        }
    }
}