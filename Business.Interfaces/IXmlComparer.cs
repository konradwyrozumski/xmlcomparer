﻿using System.Xml;

namespace Business.Interfaces
{
    public interface IXmlComparer
    {
        bool AreEqual(XmlReader leftXmlReader, XmlReader rightXmlReader);

        bool CompareXml(XmlReader leftXmlReader, XmlReader rightXmlReader, XmlWriter diffgramWriter);
    }
}
