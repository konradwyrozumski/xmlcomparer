﻿using System.IO;
using System.Xml;
using Business.Interfaces;
using Microsoft.XmlDiffPatch;
using Moq;
using NUnit.Framework;

namespace Business.Implementation.Tests
{
    public class XmlComparerTests
    {
        private const string AssemblyFolder = "Business.Implementation.Tests.TestingXmlFiles.";

        private XmlDiff xmlDiff;

        private IXmlComparer xmlComparer;

        [SetUp]
        public void Setup()
        {
            // ReSharper disable BitwiseOperatorOnEnumWithoutFlags
            xmlDiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder   |
                                  XmlDiffOptions.IgnoreNamespaces |
                                  XmlDiffOptions.IgnorePrefixes);
            // ReSharper restore BitwiseOperatorOnEnumWithoutFlags

            xmlComparer = new XmlComparer(xmlDiff);
        }

        [Test]
        public void CompareXmlForIdenticalXmlReturnsTrue()
        {
            const string resourceName = AssemblyFolder + "Simple.xml";

            var xmlWriter = new Mock<XmlWriter>();
            bool result = false;

            using (var firstStream = GetType().Assembly.GetManifestResourceStream(resourceName))
            using (var secondStream = GetType().Assembly.GetManifestResourceStream(resourceName))
            {
                if (firstStream != null && secondStream != null)
                {
                    var firstFile = XmlReader.Create(firstStream);
                    var secondFile = XmlReader.Create(secondStream);
                    result = xmlComparer.CompareXml(firstFile, secondFile, xmlWriter.Object);
                }
            }

            Assert.IsTrue(result);
        }

        [Test]
        public void CompareXmlForDifferentXmlReturnsFalse()
        {
            const string resourceName = AssemblyFolder + "Simple.xml";
            const string secondResourceName = AssemblyFolder + "SimpleDifferent.xml";
            var xmlWriter = new Mock<XmlWriter>();
            bool result = false;

            using (var firstStream = GetType().Assembly.GetManifestResourceStream(resourceName))
            using (var secondStream = GetType().Assembly.GetManifestResourceStream(secondResourceName))
            {
                if (firstStream != null && secondStream != null)
                {
                    var firstFile = XmlReader.Create(firstStream);
                    var secondFile = XmlReader.Create(secondStream);
                    result = xmlComparer.CompareXml(firstFile, secondFile, xmlWriter.Object);
                }
            }

            Assert.IsFalse(result);
        }

        [Test]
        public void CompareXmlForDifferentXmlReturnsDifferences()
        {
            const string resourceName = AssemblyFolder + "Simple.xml";
            const string secondResourceName = AssemblyFolder + "SimpleDifferent.xml";
            var sw = new StringWriter();
            XmlWriter xmlWriter = XmlWriter.Create(sw);
            bool result = false;

            using (var firstStream = GetType().Assembly.GetManifestResourceStream(resourceName))
            using (var secondStream = GetType().Assembly.GetManifestResourceStream(secondResourceName))
            {
                if (firstStream != null && secondStream != null)
                {
                    var firstFile = XmlReader.Create(firstStream);
                    var secondFile = XmlReader.Create(secondStream);
                    result = xmlComparer.CompareXml(firstFile, secondFile, xmlWriter);
                }
            }

            Assert.IsFalse(result);
        }
    }
}
